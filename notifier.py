# -*- coding: utf-8 -*-
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText


TO_EMAIL_LIST = ['shpuntik37@ya.ru', 'decemberliz92@gmail.com']


class Notifier():
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def send(self, movies):
        content = ''
        for movie in movies:
            schedule = ', '.join(movie['session_list'])
            content += '<p><b>%s</b>: %s</p>' % (movie['title'], schedule)

        html = """
            <html>
              <head></head>
              <body>
                %s
              </body>
            </html>
            """ % content

        msg = MIMEMultipart('alternative')
        msg['From'] = self.username
        msg['To'] = ', '.join(TO_EMAIL_LIST)
        msg['Subject'] = u'Фильмы с пригласительными'
        msg.attach(MIMEText(html, 'html'))

        try:
            server = smtplib.SMTP('smtp.gmail.com:587')
            server.starttls()
            server.login(self.username, self.password)
            server.sendmail(self.username, TO_EMAIL_LIST, msg.as_string())
        except Exception as e:
            raise Exception('Error when send email: %s' % e)
        finally:
            server.quit()