import requests
from pyquery import PyQuery as pq


class Parser():
    def __init__(self, url):
        self.url = url
        self.movie_list = []

    def parse(self):
        try:
            r = requests.get(self.url)
        except Exception as e:
            raise Exception('Error when parse url: %s' % e)

        d = pq(r.text)
        movie_list = d.find('.film')

        for movie in movie_list:
            parsed_session_list = []
            title = pq(movie).find('.film_name').text().encode('utf8')
            sale = pq(movie).find('.sales_invites li').eq(0)
            invite = pq(movie).find('.sales_invites li').eq(1)
            session_list = pq(movie).find('.time_price')

            for session in session_list:
                time = pq(session).find('.time').text().encode('utf8')
                parsed_session_list.append(time)

            self.movie_list.append({
                'title': title,
                'session_list': parsed_session_list,
                'sale': True if sale else False,
                'invite': True if invite else False
            })

    def get_movies_with_invite(self):
        return [movie for movie in self.movie_list if movie['invite']]